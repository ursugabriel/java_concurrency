package concurrency.introduction;

public class RunnableExample implements Runnable {

    private String nameToPrint;

    private RunnableExample(String nameToPrint) {
        this.nameToPrint = nameToPrint;
    }

    @Override
    public void run() {
        System.out.println("Thread " + Thread.currentThread().getName() + " printed " + nameToPrint);
    }

    public static void main(String[] args) {

        new Thread(new RunnableExample("Ion")).start();

        new Thread(() -> System.out.println("Thread " + Thread.currentThread().getName() + " printed Ionica")).start();


        Runnable runnable = new RunnableExample("Vasile");
        runnable.run();

        new Thread(()-> System.out.println("Thread " + Thread.currentThread().getName() + " printed Vasilica")).run();

    }
}
