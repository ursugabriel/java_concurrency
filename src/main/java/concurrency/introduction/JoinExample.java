package concurrency.introduction;

public class JoinExample {

    public static void main(String[] args) throws InterruptedException {

        Runnable task = () -> {
            String name = Thread.currentThread().getName();
            for (int i = 0; i < 5; i++) {
                System.out.println(name + " printed " + i);
            }
        };

        Thread threadOne = new Thread(task, "Thread ONE");
        Thread threadTwo = new Thread(task, "Thread TWO");


        threadOne.start();
        threadOne.join();
        threadTwo.start();
        threadTwo.join();

        System.out.println("This should be the last message if we use join");
    }
}
