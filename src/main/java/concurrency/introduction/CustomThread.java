package concurrency.introduction;

public class CustomThread extends Thread {

    private String nameToPrint;

    private CustomThread(String nameToPrint) {
        this.nameToPrint = nameToPrint;
    }

    @Override
    public void run() {
        System.out.println("Printed " + nameToPrint);
    }


    public static void main(String[] args) {

        new CustomThread("Java Threads").start();

    }
}
