package concurrency.introduction;

public class YieldExample {

    public static void main(String[] args) {

        Runnable task = () -> {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + ": Produced Item " + i);
                Thread.yield();
            }
        };

        Thread threadOne = new Thread(task, "Thread ONE");
        Thread threadTwo = new Thread(task, "Thread TWO");

        threadOne.setPriority(Thread.MIN_PRIORITY);
        threadTwo.setPriority(Thread.MAX_PRIORITY);

        threadOne.start();
        threadTwo.start();
    }
}
