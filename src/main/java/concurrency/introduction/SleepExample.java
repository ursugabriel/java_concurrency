package concurrency.introduction;

public class SleepExample {

    private static int counter = 0;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            for (int i = 0; i < 500_000_000; i++) {
                counter++;
            }
        }).start();

        while (counter < 500000000) {
            System.out.println("Not reached yet. Current counter " + counter);
            Thread.sleep(1000);
        }

        System.out.println("Reached counter " + counter);
    }
}
