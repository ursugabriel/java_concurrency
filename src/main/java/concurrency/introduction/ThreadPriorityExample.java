package concurrency.introduction;

public class ThreadPriorityExample extends Thread {

    @Override
    public void run() {
        System.out.println("Thread name: " + Thread.currentThread().getName() + ". Thread priority: " + Thread.currentThread().getPriority());
    }

    public static void main(String[] args) {

        ThreadPriorityExample thread1 = new ThreadPriorityExample();
        ThreadPriorityExample thread2 = new ThreadPriorityExample();
        ThreadPriorityExample thread3 = new ThreadPriorityExample();

        thread3.setPriority(10);
        thread1.setPriority(1);
        thread2.setPriority(2);

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
