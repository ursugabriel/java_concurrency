package concurrency.introduction;

import java.util.concurrent.*;

public class CallableExample implements Callable<Integer> {

    private int numberOne;
    private int numberTwo;

    private CallableExample(int numberOne, int numberTwo) {
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
    }

    @Override
    public Integer call() {
        return numberOne * numberTwo;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        Future<Integer> submit = executor.submit(new CallableExample(10, 10));

        System.out.println(submit);

        executor.shutdown();

    }
}
