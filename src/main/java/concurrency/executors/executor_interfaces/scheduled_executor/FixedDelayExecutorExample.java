package concurrency.executors.executor_interfaces.scheduled_executor;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FixedDelayExecutorExample {

    public static void main(String[] args) {

        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

        Runnable task4 = () -> {
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println("Current time: " + new Date(System.currentTimeMillis()));
            } catch (InterruptedException e) {
                System.err.println("Task interrupted");
            }
        };
        executorService.scheduleWithFixedDelay(task4, 0, 1, TimeUnit.SECONDS);
    }
}
