package concurrency.executors.executor_interfaces.scheduled_executor;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FixedRateSchedulerExampleOne {


    public static void main(String[] args) {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        System.out.println("The time is : " + new Date());

        executor.scheduleAtFixedRate(() -> System.out.println("The time is : " + new Date()), 2, 5, TimeUnit.SECONDS);

        try {
            TimeUnit.MILLISECONDS.sleep(20000);
        } catch (InterruptedException e) {
            System.err.println("Task interrupted");
        }

        executor.shutdown();
    }
}
