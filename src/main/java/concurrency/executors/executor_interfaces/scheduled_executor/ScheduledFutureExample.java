package concurrency.executors.executor_interfaces.scheduled_executor;

import java.util.Date;
import java.util.concurrent.*;

public class ScheduledFutureExample {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        System.out.println("Current time: " + new Date(System.currentTimeMillis()));

        Callable<Date> task = () -> new Date(System.currentTimeMillis());

        ScheduledFuture<?> future = executor.schedule(task, 3, TimeUnit.SECONDS);

        Thread.sleep(1500);
        long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
        System.out.println("Remaining milliseconds  until execution " + remainingDelay);
        System.out.println("Current time: " + future.get());

        executor.shutdown();
    }
}
