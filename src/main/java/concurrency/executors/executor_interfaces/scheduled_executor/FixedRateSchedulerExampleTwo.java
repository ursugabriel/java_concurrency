package concurrency.executors.executor_interfaces.scheduled_executor;

import java.util.concurrent.*;

public class FixedRateSchedulerExampleTwo {

    private static Integer count = 0;

    public static void main(String[] args) {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);

        Runnable task1 = () -> {
            while (count < 10) {
                try {
                    Thread.sleep(1000);
                    count++;
                } catch (InterruptedException e) {
                    System.err.println("Task interrupted");
                    executorService.shutdownNow();
                }
            }
        };

        Future<?> submit = executorService.submit(task1);
//        submit.get();

        Runnable task2 = () -> {
            if (submit.isDone()) {
                System.out.println("Task finished");
                executorService.shutdownNow();
            } else {
                System.out.println("Task not finished");
            }
        };
        int initialDelay = 0;
        int period = 2;
        executorService.scheduleAtFixedRate(task2, initialDelay, period, TimeUnit.SECONDS);


        System.out.println("I'll just do this while i'm waiting...");
    }
}
