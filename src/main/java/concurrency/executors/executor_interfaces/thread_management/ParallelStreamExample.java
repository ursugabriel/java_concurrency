package concurrency.executors.executor_interfaces.thread_management;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class ParallelStreamExample {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "5");
//
        List<Integer> numbers = getNumbers();

        numbers.parallelStream().forEach(n -> {
            try {
                Thread.sleep(5);
                System.out.println("Loop 1 : Value = " + n + " | Thread: " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                System.out.println("Thread interrupted");
            }
        });

        numbers.parallelStream().forEach(n -> {
            try {
                Thread.sleep(5);
                System.out.println("Loop 2 : Value = " + n + " | Thread: " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                System.out.println("Thread interrupted");
            }
        });


        ForkJoinPool forkJoinPool = new ForkJoinPool(4);
        forkJoinPool.submit(() -> numbers.parallelStream().forEach(n -> {
            try {
                Thread.sleep(5);
                System.out.println("Loop 3 : Value = " + n + " | Thread: " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                System.out.println("Thread interrupted");
            }
        })).get();


        ForkJoinPool forkJoinPool2 = new ForkJoinPool(4);
        forkJoinPool2.submit(() -> numbers.parallelStream().forEach(n -> {
            try {
                Thread.sleep(5);
                System.out.println("Loop 4 : Value = " + n + " | Thread: " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                System.out.println("Thread interrupted");
            }
        })).get();


    }

    private static List<Integer> getNumbers() {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i <= 5; i++)
            numbers.add(i);
        return Collections.unmodifiableList(numbers);
    }
}