package concurrency.executors.executor_interfaces.thread_management;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.stream.IntStream;

public class SemaphoreExample {

    private int counter =0;

    private void increment(){
        counter++;
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        SemaphoreExample example = new SemaphoreExample();

        Semaphore semaphore = new Semaphore(5);

        IntStream.range(0, 100).forEach(count -> executor.submit(() -> {
            try {
                semaphore.acquire();
                synchronized (example){
                example.increment();
                System.out.println(Thread.currentThread().getName() + " and printed " + example.counter);}
                Thread.sleep(2000);

            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            } finally {
                    semaphore.release();

            }
        }));

        executor.shutdown();
    }
}
