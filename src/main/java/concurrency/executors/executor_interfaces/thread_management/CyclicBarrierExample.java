package concurrency.executors.executor_interfaces.thread_management;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class CyclicBarrierExample {


    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(4);

        // this means that at least 8 threads needs to reach the cycle barrier in order to continue;
        CyclicBarrier firstBarrier = new CyclicBarrier(4, () -> System.out.println("All services initialized"));
        CyclicBarrier secondBarrier = new CyclicBarrier(4, () -> System.out.println("All services started"));
        CyclicBarrier finalBarrier = new CyclicBarrier(4, () -> System.out.println("Running app..."));


        System.out.println("Service started...");
        IntStream.rangeClosed(1, 4).forEach(value -> {
            executor.submit(() -> {
                try {
                    System.out.println(Thread.currentThread().getName() + " is initializing...");
                    firstBarrier.await();
                    System.out.println(Thread.currentThread().getName() + " is starting...");
                    secondBarrier.await();
                    System.out.println(Thread.currentThread().getName() + " started.");
                    finalBarrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    Thread.currentThread().interrupt();
                }
            });
        });

        executor.shutdown();
    }
}

