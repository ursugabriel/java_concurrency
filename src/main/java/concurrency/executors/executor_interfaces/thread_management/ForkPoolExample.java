package concurrency.executors.executor_interfaces.thread_management;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

public class ForkPoolExample extends RecursiveTask<Long> {

    private final long[] numbers;
    private final int start;
    private final int end;
    public static final long thresHold = 10_000;

    public ForkPoolExample(long[] numbers) {
        this(numbers, 0, numbers.length);
    }

    public ForkPoolExample(long[] numbers, int start, int end) {
        this.numbers = numbers;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {

        int length = end - start;
        if (length <= thresHold) {
            return add();
        }

        ForkPoolExample firstTask = new ForkPoolExample(numbers, start, start + length / 2);
        //start asynchronously
        firstTask.fork();

        ForkPoolExample secondTask = new ForkPoolExample(numbers, start + length / 2, end);

        Long secondTaskResult = secondTask.compute();
        Long firstTaskResult = firstTask.join();

        return firstTaskResult + secondTaskResult;

    }

    private long add() {
        long result = 0;
        for (int i = start; i < end; i++) {
            result += numbers[i];
        }
        return result;
    }

    public static long startForkJoinSum(long n) {
        long[] numbers = LongStream.rangeClosed(1, n).toArray();
        ForkJoinTask<Long> task = new ForkPoolExample(numbers);
        return new ForkJoinPool().invoke(task);
    }

    public static void main(String[] args) {
        System.out.println(ForkPoolExample.startForkJoinSum(1_000_000));
    }

}