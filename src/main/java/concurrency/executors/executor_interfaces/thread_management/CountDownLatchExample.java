package concurrency.executors.executor_interfaces.thread_management;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CountDownLatchExample {

    public static void main(String args[]) throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(3);

        // latch is set to 3.. each time a task will finish it's job, it will be decreased... once it reaches 0, it will continue to do the main job
        CountDownLatch latch = new CountDownLatch(3);

        Runnable taskOne = () -> {
            try {
                Thread.sleep(2000);
                latch.countDown();
                System.out.println(Thread.currentThread().getName() + " finished");
            } catch (InterruptedException e) {
                System.err.println("Worker interrupted");
            }
        };

        Runnable taskTwo = () -> {
            try {
                Thread.sleep(8000);
                latch.countDown();
                System.out.println(Thread.currentThread().getName() + " finished");
            } catch (InterruptedException e) {
                System.err.println("Worker interrupted");
            }
        };

        Runnable taskThree = () -> {
            try {
                Thread.sleep(4000);
                latch.countDown();
                System.out.println(Thread.currentThread().getName() + " finished");
            } catch (InterruptedException e) {
                System.err.println("Worker interrupted");
            }
        };

        executor.submit(taskOne);
        executor.submit(taskTwo);
        executor.submit(taskThree);

        latch.await(5000, TimeUnit.SECONDS);

        System.out.println(Thread.currentThread().getName() + " has finished");

        executor.shutdown();
    }
}
