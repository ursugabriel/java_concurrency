package concurrency.executors.executor_interfaces.executor_service.submit;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorSubmitAndWaitForResponsesCallable {

    private static String returnMessage() {
        return "--printing message--";
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Callable<String> callableOne = () -> {
            System.out.println("............................................................");
            System.out.println("Callable 1 started");
            String message = returnMessage();
            System.out.println("Callable 1 finished");
            System.out.println("............................................................");
            return message;
        };

        Callable<String> callableTwo = () -> {
            System.out.println("............................................................");
            System.out.println("Callable 2 started");
            String message = returnMessage();
            System.out.println("Callable 2 finished");
            System.out.println("............................................................");
            return message;
        };


        Future<?> submit = executorService.submit(callableOne);
        System.out.println(submit.get());


        List<Callable<String>> callables = Arrays.asList(callableOne, callableTwo);
        List<Future<String>> futures = executorService.invokeAll(callables);

        for (Future future : futures) {
            System.out.println(future.get());
        }

        String result = executorService.invokeAny(callables);
        System.out.println(result);


        executorService.shutdown();
    }
}
