package concurrency.executors.executor_interfaces.executor_service.submit;

import java.util.concurrent.*;

public class ExecutorSubmitAndWaitForResponsesRunnable {

    private static void printMessage() throws InterruptedException {
        Thread.sleep(1000);
        System.out.println("--printing message--");
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Runnable runnableOne = () -> {
            System.out.println("............................................................");
            System.out.println("Runnable started");
            try {
                printMessage();
                System.out.println("Runnable finished");
            } catch (InterruptedException e) {
                System.err.println("Task interrupted");
            }
            System.out.println("............................................................");
        };


        executorService.execute(runnableOne);

        Future<?> submit = executorService.submit(runnableOne);
        System.out.println("Task status without get(): " + submit.isDone());


        Future<?> submit2 = executorService.submit(runnableOne);
        submit2.get();
        System.out.println("Task status with get(): " + submit2.isDone());


        Future<?> submit3 = executorService.submit(runnableOne);
        try {
            submit3.get(500, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            System.out.println("Waiting time expired. Task is done?: " + submit3.isDone());
            submit3.cancel(true);
            System.out.println("Task is canceled?: " + submit3.isCancelled());
        }

        try {
            System.out.println(submit3.get());
        } catch (CancellationException e) {
            System.out.println("Failed to get response of the task because it was cancelled");
        }

        executorService.shutdown();
    }
}
