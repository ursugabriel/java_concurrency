package concurrency.executors.executor_interfaces.executor_service.shutdown;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorShutdownExample {

    private static void printNumbers() {
        int count = 0;
        while (count <= 10) {
            System.out.println(count);
            count++;
        }
    }

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Runnable runnableOne = () -> {
            System.out.println("Task 1 started");
            printNumbers();
            System.out.println("Task 1 finished");
        };
        Runnable runnableTwo = () -> {
            System.out.println("Task 2 started");
            printNumbers();
            System.out.println("Task 2 finished");
        };

        Runnable runnableThree = () -> {
            System.out.println("Task 2 started");
            printNumbers();
            System.out.println("Task 2 finished");
        };

        executorService.submit(runnableOne);
        executorService.submit(runnableTwo);
        executorService.submit(runnableThree);

        // will wait for tasks to complete
//        executorService.shutdown();

        //will attempt to stop the remaining tasks
        List<Runnable> tasksNeverStarted = executorService.shutdownNow();
//        System.out.println("Tasks never started " + tasksNeverStarted.size());
    }


}
