package concurrency.executors.executor_interfaces.executor_service.shutdown;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorAwaitTermination {

    private static void printName(String name) throws InterruptedException {
        Thread.sleep(5000);
        System.out.println("Name printed: " + name);
    }

    public static void main(String[] args) {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        Runnable runnableOne = () -> {
            System.out.println("Task 1 started");
            try {
                printName("Ion");
                System.out.println("Task 1 finished");
            } catch (InterruptedException e) {
                System.err.println("Task 1 interrupted");
            }
        };

        executor.submit(runnableOne);

        try {
            executor.shutdown();
            boolean terminated = executor.awaitTermination(2, TimeUnit.SECONDS);
            if (!terminated) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            System.err.println("Tasks interrupted");
        } finally {
            executor.shutdownNow();
        }
    }
}
