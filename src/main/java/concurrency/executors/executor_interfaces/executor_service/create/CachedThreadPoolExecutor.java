package concurrency.executors.executor_interfaces.executor_service.create;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CachedThreadPoolExecutor {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();

        Runnable task = () -> System.out.println("Thread " + Thread.currentThread().getName() + " called");

        executorService.submit(task);
        executorService.submit(task);
        executorService.submit(task);
    }
}
