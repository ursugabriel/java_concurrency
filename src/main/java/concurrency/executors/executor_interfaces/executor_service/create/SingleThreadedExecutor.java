package concurrency.executors.executor_interfaces.executor_service.create;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleThreadedExecutor {

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Runnable task = () -> {
            for (int i = 0; i <= 5; i++) {
                System.out.println("Thread " + Thread.currentThread().getName() + " printed value: " + i);
            }
        };

        executorService.submit(task);
        executorService.submit(task);

        // the message will be show while the task is still running because the SleepExample thread is not a part of the Executor service;
        System.out.println("Message in main thread ....");
    }
}
