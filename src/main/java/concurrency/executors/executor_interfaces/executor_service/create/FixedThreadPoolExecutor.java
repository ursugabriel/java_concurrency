package concurrency.executors.executor_interfaces.executor_service.create;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPoolExecutor {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        Runnable task = () -> System.out.println("Thread " + Thread.currentThread().getName() + " called");

        executorService.submit(task);
        executorService.submit(task);
        executorService.submit(task);

    }
}
