package concurrency.executors.executor_interfaces.executor;


import java.util.concurrent.Executor;

public class ExecutorExample implements Executor {

    @Override
    public void execute(Runnable command) {
        new Thread(command).start();
    }

    public static void main(String[] args) {
        ExecutorExample executor = new ExecutorExample();
        executor.execute(() -> System.out.println("Executor is working."));
    }
}
