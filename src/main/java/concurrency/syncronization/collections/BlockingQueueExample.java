package concurrency.syncronization.collections;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueExample {


    public static void main(String[] args) {

        int poisonPill = -1;
        BlockingQueue<Integer> queue = new LinkedBlockingQueue<>(3);


        Runnable producer = new Thread(() -> {
            try {
                for (int i = 1; i <10; i++) {
                    System.out.println("[Producer] Put : " + i);
                    try {
                        queue.put(i);
                        System.out.println("[Producer] Queue remainingCapacity : " + queue.remainingCapacity());
                    } catch (InterruptedException e) {
                        System.err.println("Thread interrupted");
                    }
                }
            } finally {
                while (true) {
                    try {
                        queue.put(poisonPill);
                        break;
                    } catch (InterruptedException e) {
                        System.err.println("Thread interrupted");
                    }
                }
            }

        });

        Runnable consumer = new Thread(() -> {
            try {
                while (true) {

                    Integer take = queue.take();

                    System.out.println("[Consumer] Take : " + take);

                    if (take == poisonPill) {
                        break;
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });

        new Thread(producer).start();
        new Thread(consumer).start();
    }
}
