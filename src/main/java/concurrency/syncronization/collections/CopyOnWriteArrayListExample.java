package concurrency.syncronization.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CopyOnWriteArrayListExample {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executor = Executors.newFixedThreadPool(1);

        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);

        System.out.println("List before the thread: " + arrayList);

        Future<?> submit = executor.submit(() -> {
            for (Integer integer : arrayList) {
                if (integer % 2 == 0) {
                    arrayList.remove(integer);
                }
            }
            System.out.println("List inside the thread: " + arrayList);
        });

        submit.get();

        System.out.println("List after the thread: " + arrayList);

        executor.shutdown();

    }
}
