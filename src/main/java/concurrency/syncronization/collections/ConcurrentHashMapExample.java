package concurrency.syncronization.collections;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class ConcurrentHashMapExample {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = Executors.newFixedThreadPool(2);

        Map<String, Integer> map = new ConcurrentHashMap<>();

        Runnable task = () -> IntStream.range(0, 10).forEach(value -> {
            map.putIfAbsent("A" + value, value);
            System.out.println(Thread.currentThread().getName() + " added " + map.get("A" + value));
        });

        Runnable task2 = () -> IntStream.range(0, 10).forEach(value -> {
            map.putIfAbsent("B" + value, value);
            System.out.println(Thread.currentThread().getName() + " added " + map.get("B" + value));
        });

        executor.submit(task);
        executor.submit(task2);

        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);


    }
}
