package concurrency.syncronization.blocks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class SynchronizedExample {

    private int count = 0;

    private void increment() {
        count++;
        System.out.println(Thread.currentThread().getName() + " incremented to " + count);
    }

    private void synchronizedInsideIncrement() {
        synchronized (this) {
            count++;
            System.out.println(Thread.currentThread().getName() + " incremented to " + count);
        }
    }

    private synchronized void synchronizedIncrement() {
        count++;
        System.out.println(Thread.currentThread().getName() + " incremented to " + count);
    }

    public static void main(String[] args) {
        SynchronizedExample synchronizedExample = new SynchronizedExample();

        ExecutorService executor = Executors.newFixedThreadPool(4);
        IntStream.range(0, 20).forEach(count -> executor.submit(synchronizedExample::synchronizedInsideIncrement));

        executor.shutdown();
        try {
            executor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.err.println("Task interrupted");
        }
        System.out.println("Count: " + synchronizedExample.count);
    }
}