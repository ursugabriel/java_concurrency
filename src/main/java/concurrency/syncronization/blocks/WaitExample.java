package concurrency.syncronization.blocks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WaitExample {


    private boolean pizzaArrived = false;
    private boolean pizzaOrdered = false;


    private void orderPizza() {
        synchronized (this) {
            this.pizzaOrdered = true;
            System.out.println(Thread.currentThread().getName() + " just ordered pizza ...");
            notifyAll();
        }
    }

    private void bringPizza() throws InterruptedException {
        synchronized (this) {
            while (!pizzaOrdered) {
                wait();
            }
            this.pizzaArrived = true;
            System.out.println(Thread.currentThread().getName() + " just arrived with pizza ...");
            notifyAll();
        }
    }

    private void eatPizza() throws InterruptedException {
        synchronized (this) {
            while (!pizzaArrived) {
                wait();
            }
        }
        System.out.println(Thread.currentThread().getName() + " is eating pizza ...");
    }


    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);

        WaitExample example = new WaitExample();

        executor.submit(example::orderPizza);

        executor.submit(() -> {
            try {
                Thread.sleep(5000);
                example.bringPizza();
            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            }

        });

        executor.submit(() -> {
            try {
                example.eatPizza();
            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            }
        });


        executor.shutdown();
    }
}