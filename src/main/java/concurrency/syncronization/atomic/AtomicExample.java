package concurrency.syncronization.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class AtomicExample {

    private AtomicInteger atomicInteger = new AtomicInteger();

    private void update() {
        System.out.println(Thread.currentThread().getName() + " incremented to " + atomicInteger.incrementAndGet());
    }

    public static void main(String[] args) {
        AtomicExample atomic = new AtomicExample();

        ExecutorService executor = Executors.newFixedThreadPool(4);

        IntStream.range(0, 10).forEach(count -> executor.submit(atomic::update));

        executor.shutdown();
    }
}