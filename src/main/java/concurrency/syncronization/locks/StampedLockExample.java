package concurrency.syncronization.locks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.StampedLock;

public class StampedLockExample {

    private int count = 0;

    private void increment() {
        count++;
        System.out.println(Thread.currentThread().getName() + " incremented to " + count);
    }

    public int getCount() {
        return count;
    }

    public static void main(String[] args) {
        StampedLockExample counter = new StampedLockExample();

        ExecutorService executor = Executors.newFixedThreadPool(5);

        StampedLock lock = new StampedLock();

        executor.submit(() -> {
            long stamp = lock.writeLock();
            try {
                Thread.sleep(2000);
                System.out.println(Thread.currentThread().getName() + " obtained write lock");
                counter.increment();
            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            } finally {
                lock.unlockWrite(stamp);
            }
        });

        Runnable task = () -> {
            long stamp = lock.readLock();
            try {
                System.out.println("Final value value " + counter.getCount());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            } finally {
                System.out.println(lock.validate(stamp));
                lock.unlockRead(stamp);
            }
        };

        executor.submit(task);
        executor.submit(task);

        executor.shutdown();
    }
}
