package concurrency.syncronization.locks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.IntStream;

public class ReadWriteLockExample {

    private int count = 0;

    private void increment() {
        count++;
        System.out.println(Thread.currentThread().getName() + " incremented to " + count);
    }

    public int getCount() {
        return count;
    }

    public static void main(String[] args) {
        ReadWriteLockExample counter = new ReadWriteLockExample();

        ExecutorService executor = Executors.newFixedThreadPool(5);

        ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

        IntStream.range(0, 10).forEach(count -> executor.submit(() -> {
            try {
                lock.writeLock().lock();
                System.out.println(Thread.currentThread().getName() + " obtained write lock");
                counter.increment();
            } finally {
                if (lock.isWriteLockedByCurrentThread()) {
                    lock.writeLock().unlock();
                }
            }
        }));

        Runnable task = () -> {
            lock.readLock().lock();
            try {
                System.out.println("Final value value " + counter.getCount());
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            } finally {
                lock.readLock().unlock();
            }
        };

        executor.submit(task);
        executor.submit(task);

        executor.shutdown();
    }
}
