package concurrency.syncronization.locks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class ReentrantLockExample {

    private int count = 0;

    private void increment() {
        count++;
        System.out.println(Thread.currentThread().getName() + " incremented to " + count);
    }


    public static void main(String[] args) {
        ReentrantLockExample counter = new ReentrantLockExample();

        ReentrantLock lock = new ReentrantLock();

        ExecutorService executor = Executors.newFixedThreadPool(5);

        IntStream.range(0, 10).forEach(count -> executor.submit(() -> {
            try {
                lock.lock();
                System.out.println(Thread.currentThread().getName() + " obtained lock");
                counter.increment();
            } finally {
                if (lock.isHeldByCurrentThread()) {
                    lock.unlock();
                }
            }
        }));

        executor.shutdown();
    }
}
