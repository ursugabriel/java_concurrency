package concurrency.liveness;

public class DeadlockExampleTwo {

    public static void main(String[] args) throws InterruptedException {

        Thread mainThread = Thread.currentThread();

        Thread threadOne = new Thread(() -> {
            System.out.println("Waiting for main thread completion");
            try {
                mainThread.join();
            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            }
        });


        threadOne.start();
        System.out.println("Main thread waiting for Thread One completion");

        threadOne.join();

        System.out.println("Main thread execution completes");
    }
}