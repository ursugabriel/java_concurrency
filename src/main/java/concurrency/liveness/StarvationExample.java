package concurrency.liveness;

public class StarvationExample {

    private static final Object objectOne = new Object();

    public static void main(String[] args) {

        Thread threadOne = new Thread(() -> {
            synchronized (objectOne) {
                try {
                    for (int i = 0; i <= 10; i++) {
                        Thread.sleep(1000);
                        System.out.println("Thread 1: Holding lock...");
                    }
                } catch (InterruptedException e) {
                    System.err.println("Thread interrupted");
                }
            }
        });

        Thread threadTwo = new Thread(() -> {
            synchronized (objectOne) {
                System.out.println("Thread 2: Holding lock...");
            }
        });

        threadOne.start();
        threadTwo.start();
    }
}
