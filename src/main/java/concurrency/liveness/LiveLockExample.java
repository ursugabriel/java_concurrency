package concurrency.liveness;

public class LiveLockExample {

    public static void main(String[] args) {

        Person girl = new Person("Maria ");
        Person boy = new Person("Andrei");


        Thread threadOne = new Thread(() -> girl.sayHungUp(boy));
        threadOne.start();

        Thread threadTwo = new Thread(() -> boy.sayHungUp(girl));
        threadTwo.start();
    }
}


class Person {

    private String name;
    private boolean saidGoodBye = false;

    Person(String name) {
        this.name = name;
    }

    void sayHungUp(Person person) {
        while (!person.isHungUp()) {

            System.out.println(name + " said : you hung up");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.err.println("Thread interrupted");
            }
        }

        System.out.println(name + " hung up");

        this.saidGoodBye = true;
    }

    private boolean isHungUp() {
        return this.saidGoodBye;
    }
}
