package concurrency.liveness;

public class DeadlockExample {

    private static final Object objectOne = new Object();
    private static final Object objectTwo = new Object();

    public static void main(String[] args) {

        Thread threadOne = new Thread(() -> {
            synchronized (objectOne) {
                System.out.println("Thread 1: Holding lock 1...");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.err.println("Thread interrupted");
                }
                System.out.println("Thread 1: Waiting for lock 2...");

                synchronized (objectTwo) {
                    System.out.println("Thread 1: Holding lock 1 & 2...");
                }
            }
        });

        Thread threadTwo = new Thread(() -> {
            synchronized (objectOne) {
                System.out.println("Thread 2: Holding lock 2...");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.err.println("Thread interrupted");
                }
                System.out.println("Thread 2: Waiting for lock 1...");

                synchronized (objectTwo) {
                    System.out.println("Thread 2: Holding lock 1 & 2...");
                }
            }
        });

        threadOne.start();
        threadTwo.start();
    }
}